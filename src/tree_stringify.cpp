#include "tree_stringify.h"

#include "tree.h"


TreeStringify::TreeStringify(std::ostream &out_stream, signed char ident)
    : m_out_stream(out_stream),
      m_ident(ident)
{
}

TreeStringify::~TreeStringify()
{

}

inline void write_ident(int ident, std::ostream & out_stream)
{
    for(int i = 0; i < ident; ++i)
        out_stream << ' ';
}

void TreeStringify::visit(TreeNode *node_ptr, int depth)
{

    write_ident(depth * m_ident, m_out_stream);

    if(node_ptr->type() == typeid(int))
    {
        m_out_stream <<  node_ptr->extract_int();
    }
    else if(node_ptr->type() == typeid(float))
    {
        m_out_stream <<  node_ptr->extract_float();
    }
    else if(node_ptr->type()  == typeid(std::string))
    {
        m_out_stream << node_ptr->extract_string();
    } else
    {
        throw std::runtime_error("Wrong value type:" +
                                 std::string(node_ptr->type().name()));
    }

    m_out_stream << std::endl;

}

void TreeStringify::leave([[maybe_unused]] TreeNode *node_ptr,
                          [[maybe_unused]] int depth)
{
}
