#include "tree_walker.h"

#include <list>
#include <stdexcept>

#include "tree.h"
#include "node_visiter.h"

void TreeWalker::walk(TreeNode &tree,
                              NodeVisiter & visiter)
{
    struct stack_item
    {
        TreeNode * node;
        bool finish = false;
    };

    std::list<stack_item> stack;
    stack.push_back({
                        .node = &tree,
                        .finish = false
                    });
    stack_item * current = nullptr;
    int depth = 0;
    while(!stack.empty())
    {
        current = &stack.back();

        const auto & items = current->node->items();

        if(current->finish)
        {
            stack.pop_back();
            if(!items.empty())
            {
                --depth;
            }
            visiter.leave(current->node, depth);
            continue;
        }

        visiter.visit(current->node, depth);

        depth += !items.empty();
// revers for save order of items
        for(auto r_it = items.rbegin(); r_it != items.rend(); ++r_it)
        {
            stack.push_back({
                                .node = r_it->get(),
                                .finish = false
                            });
        }
        current->finish = true;
    }
}
