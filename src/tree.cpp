#include "tree.h"

#include <assert.h>

TreeNode::TreeNode(int val) noexcept
    : m_value(val)
{
}

TreeNode::TreeNode(float val) noexcept
    : m_value(val)
{
}

TreeNode::TreeNode(const std::string &val) noexcept
    : m_value(val)
{
}

void TreeNode::set(int val) noexcept
{
    m_value = val;
}

void TreeNode::set(float val) noexcept
{
    m_value = val;
}

void TreeNode::set(const std::string &val) noexcept
{
    m_value = val;
}

int TreeNode::extract_int() const
{
    return std::any_cast<int>(m_value);
}

float TreeNode::extract_float() const
{
    return std::any_cast<float>(m_value);
}

std::string TreeNode::extract_string() const
{
    return std::any_cast<std::string>(m_value);
}

const std::vector<TreeNode::Ptr> &TreeNode::items() const noexcept
{
    return m_items;
}

void TreeNode::push_item_value(TreeNode::Ptr item)
{
    if(!item)
    {
        throw std::runtime_error("item is empty");
    }
    m_items.push_back(item);
}
