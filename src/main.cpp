#include <iostream>
#include <list>
#include <sstream>
#include <fstream>

#include "args_parser.h"
#include "tree.h"
#include "tree_walker.h"
#include "tree_stringify.h"
#include "tree_binary_writer.h"
#include "tree_binary_reader.h"

//------------------------------------------------------------------------------
TreeNode::Ptr hardcoded_tree()
{
    auto tree = std::make_shared<TreeNode>(0.1f);
    tree->push_item_value("test_str")->push_item_value("inferno");
    tree->push_item_value(23);
    auto temp = tree->push_item_value(12.f)->push_item_value(2020.1f);
    temp->push_item_value("HelloWorld");
    temp->push_item_value(1924);
    return tree;
}

//------------------------------------------------------------------------------
TreeNode::Ptr read_from_file(const std::string & file_name)
{
    std::ifstream input_file(file_name);
    if(!input_file.is_open())
    {
        std::cout << "Can't open file:" + file_name << std::endl;
        abort();
    }

    std::cout << "Input file:" << file_name << std::endl;

    TreeBinaryReader reader;

    TreeNode::Ptr tree;

    try {
        tree = reader.read(input_file);
    } catch (const std::runtime_error & e) {
        std::cout << "Can't read data: " << e.what() << std::endl;
        input_file.close();
        abort();
    }

    input_file.close();
    return tree;
}

//------------------------------------------------------------------------------
void write_to_file(TreeNode::Ptr tree, const std::string & file_name)
{
    std::ofstream output_file(file_name);
    if(!output_file.is_open())
    {
        std::cout << "Can't open output file:" << file_name << std::endl;
        abort();
    }

    std::cout << "Output file:" << file_name << std::endl;

    TreeBinaryWriter writer(output_file);
    TreeWalker walker;
    walker.walk(*tree, writer);

    output_file.close();
}

//------------------------------------------------------------------------------
void print_tree(TreeNode::Ptr tree)
{
    TreeStringify stringify(std::cout, 4);

    TreeWalker walker;
    walker.walk(*tree, stringify);
}

//------------------------------------------------------------------------------
void print_help()
{
    std::cout << " -i <input file name> -o <output file name>\n";
}

//------------------------------------------------------------------------------
int main(int argn, char ** argv)
{
    ArgsParser parser(argn, argv);

    const std::map<std::string, std::string> & args = parser.get();
    //print help
    auto it = args.find("-h");
    if(it != args.end())
    {
        print_help();
        return 0;
    }

    TreeNode::Ptr tree;
    //handling '-i'
    it = args.find("-i");
    if(it == args.end())
    {
        std::cout << "'-i' argument is not exists. generate tree" << std::endl;
        tree = hardcoded_tree();
    }
    else
    {
       tree = read_from_file(it->second);
    }

    //print
    print_tree(tree);

    //handling '-o'
    it = args.find("-o");
    if(it == args.end())
    {
        std::cout << "'-o' argument is not exists" << std::endl;
        return 1;
    }

    write_to_file(tree, it->second);

    return 0;
}
