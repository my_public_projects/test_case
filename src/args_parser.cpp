#include "args_parser.h"

ArgsParser::ArgsParser(int argn, char **argv)
{
    std::string arg_name;
    std::string arg_value;
    m_args.insert(std::pair("command", argv[0]));

    int i = 1;

    while(i < argn)
    {
        std::string str_argv(argv[i]);
        if(str_argv[0] == '-')
        {
            arg_name = str_argv;
            arg_value = i < argn-1 ? argv[++i] : "";

             m_args.insert(std::pair(arg_name, arg_value));

        }

        ++i;
    }
}
