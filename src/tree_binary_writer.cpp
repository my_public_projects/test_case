#include "tree_binary_writer.h"
#include "tree.h"
#include "type_id.h"


TreeBinaryWriter::TreeBinaryWriter(std::ostream &out_stream)
    : m_out_stream(out_stream)
{}

TreeBinaryWriter::~TreeBinaryWriter()
{

}



void TreeBinaryWriter::visit(TreeNode *node_ptr, [[maybe_unused]] int depth)
{
    if(node_ptr->type() == typeid(int))
    {
        write(char(type_id::_int));
        write(node_ptr->extract_int());
    }
    else if(node_ptr->type() == typeid(float))
    {
        write(char(type_id::_float));
        write(node_ptr->extract_float());
    }
    else if(node_ptr->type()  == typeid(std::string))
    {
        write(char(type_id::_str));
        write(node_ptr->extract_string());
    } else
    {
        throw std::runtime_error("Wrong value type:" +
                                 std::string(node_ptr->type().name()));
    }

    write(int(node_ptr->items().size()));

}

void TreeBinaryWriter::leave([[maybe_unused]] TreeNode *node_ptr,
                             [[maybe_unused]] int depth)
{

}

void TreeBinaryWriter::write(char data)
{
    m_out_stream.put(data);
}

void TreeBinaryWriter::write(int data)
{
    m_out_stream.write(reinterpret_cast<char*>(&data), sizeof (int));
}

void TreeBinaryWriter::write(float data)
{
    m_out_stream.write(reinterpret_cast<char*>(&data), sizeof(float));
}

void TreeBinaryWriter::write(const std::string &data)
{
    m_out_stream.write(data.data(), data.size());
    m_out_stream.put(0);
}

