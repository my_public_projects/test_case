#include "tree_binary_reader.h"

#include <list>

TreeNode::Ptr TreeBinaryReader::read(std::istream & in_stream)
{
    struct stack_item
    {
        TreeNode::Ptr node;
        int children_num = 0;
    };

    std::list<stack_item> stack;

    stack.push_back({
                        .node = read_node_val(in_stream),
                        .children_num = read_int(in_stream)
                    });

    TreeNode::Ptr head = stack.back().node;

    stack_item * current = nullptr;

    while (!stack.empty() && !in_stream.eof())
    {
        current = &stack.back();
        if(current->children_num > 0)
        {
            --current->children_num;
            stack.push_back({
                                .node = read_node_val(in_stream),
                                .children_num = read_int(in_stream)
                            });
            current->node->push_item_value(stack.back().node);
            continue;
        }

        stack.pop_back();

    }

    return head;

}

char TreeBinaryReader::read_char(std::istream & in_stream)
{
    char byte;
    if(!in_stream.get(byte).good())
    {
        throw std::runtime_error("Broken stream data");
    }
    return byte;
}

int TreeBinaryReader::read_int(std::istream & in_stream)
{
    int data;
    if(!in_stream.read(reinterpret_cast<char*>(&data), sizeof(int)).good())
    {
        throw std::runtime_error("Broken stream data");
    }
    return data;
}

float TreeBinaryReader::read_float(std::istream & in_stream)
{
    float data;
    if(!in_stream.read(reinterpret_cast<char*>(&data), sizeof(float)).good())
    {
        throw std::runtime_error("Broken stream data");
    }
    return data;
}

std::string TreeBinaryReader::read_str(std::istream & in_stream)
{
    std::string str;
    char byte;
    in_stream.get(byte);
    while(in_stream.good() && byte != 0)
    {
        str.push_back(byte);
        in_stream.get(byte);
    }
    if(!in_stream.good())
    {
        throw std::runtime_error("Broken stream data");
    }

    return str;

}

type_id TreeBinaryReader::read_type_id(std::istream &in_stream)
{
    char byte = read_char(in_stream);
    type_id type = *reinterpret_cast<type_id*>(&byte);

    if(type > type_id::_str)
    {
        throw std::runtime_error("Broken stream data, undefined type_id");
    }

    return type;
}

TreeNode::Ptr TreeBinaryReader::read_node_val(std::istream &in_stream)
{
    type_id type = read_type_id(in_stream);
    switch (type) {
        case type_id::_int :
            return std::make_shared<TreeNode>(read_int(in_stream));

        case type_id::_float :
            return std::make_shared<TreeNode>(read_float(in_stream));

        case type_id::_str :
            return std::make_shared<TreeNode>(read_str(in_stream));
    }
    throw std::runtime_error("Unhandled type_id");
    return nullptr;
}
