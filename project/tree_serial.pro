TEMPLATE = app
QMAKE_CXXFLAGS_GNUCXX11 = -std=c++17
QMAKE_CXXFLAGS_GNUCXX1Z = -std=c++17
CONFIG += c++17
unix: QMAKE_CXXFLAGS += -std=c++17
unix: QMAKE_CXXFLAGS += -Wno-unknown-pragmas
unix: QMAKE_CXXFLAGS += -Wno-unused-parameter
unix: QMAKE_CXXFLAGS += -Wno-sign-compare
unix: QMAKE_CXX = g++-8
CONFIG -= app_bundle
CONFIG -= qt
TARGET = "tree_serial"
DESTDIR = $$PWD/../bin/


INCLUDEPATH += $$PWD/../include

SOURCES += \
        ../src/args_parser.cpp \
        ../src/main.cpp \
        ../src/node_visiter.cpp \
        ../src/tree.cpp \
        ../src/tree_binary_reader.cpp \
        ../src/tree_binary_writer.cpp \
        ../src/tree_stringify.cpp \
        ../src/tree_walker.cpp

HEADERS += \
    ../include/args_parser.h \
    ../include/node_visiter.h \
    ../include/tree.h \
    ../include/tree_binary_reader.h \
    ../include/tree_binary_writer.h \
    ../include/tree_stringify.h \
    ../include/tree_walker.h \
    ../include/type_id.h
