#ifndef TREE_H
#define TREE_H
#include <vector>
#include <any>
#include <string>
#include <stdexcept>
#include <memory>


/*!
 * \brief The TreeNode class
 * \details This class represents Tree structure.
 * Nodes of Tree can holds one of 3 types(int, float, std::string)
 * avoid circular reference!
 */

class TreeNode
{
public:
    using Ptr = std::shared_ptr<TreeNode>;

public:

    /*!
     * \brief TreeNode
     * Constructs TreeNode with int value
     *
     * \param val value for store
     *
     */
    TreeNode(int val) noexcept;

    /*!
     * \brief TreeNode
     * Constructs TreeNode with float value
     *
     * \param val value for store
     *
     */
    TreeNode(float val) noexcept;

    /*!
     * \brief TreeNode
     * Constructs TreeNode with std::string value
     *
     * \param val value for store
     *
     */
    TreeNode(const std::string & val) noexcept;

    /*!
     * \brief set sets int value
     *
     * \param val new value
     */
    void set(int val) noexcept;

    /*!
     * \brief set sets float value
     *
     * \param val new value
     */
    void set(float val) noexcept;

    /*!
     * \brief set sets std::string value
     *
     * \param val new value
     */
    void set(const std::string & val) noexcept;


    /*!
     * \brief extract_int
     * \return Node value of int
     * \exception std::bad_any_cast if typeid of storing value is not int
     */
    int extract_int()  const;


    /*!
     * \brief extract_float
     * \return Node value of float
     * \exception std::bad_any_cast if typeid of storing value is not float
     */
    float extract_float()  const;

    /*!
     * \brief extract_string
     * \return value of int
     * \exception std::bad_any_cast if typeid of storing value is not
     * std::string
     */
    std::string extract_string()  const;


    /*!
     * \brief value
     * \return const ref to std::any
     */
    inline const std::any & value() const noexcept
    {
        return m_value;
    }

    /*!
     * \brief type
     * \return const std::type_info ref to storing value
     */
    inline const std::type_info & type() const noexcept
    {
        return m_value.type();
    }


    /*!
     * \brief items
     * \return const ref to vector of sub nodes
     */
    const std::vector<TreeNode::Ptr> & items() const noexcept;


    /*!
     * \brief push_item_value creates sub Node of Tree with \a item_val
     * \param item_val int value for store
     * \return std::shared_ptr<TreeNode> aka TreeNode::Ptr to the new sub node
     */
    inline TreeNode::Ptr push_item_value(int item_val) noexcept
    {
        return push(item_val);
    }

    /*!
     * \brief push_item_value creates sub Node of Tree with \a item_val
     * \param item_val float value for store
     * \return std::shared_ptr<TreeNode> aka TreeNode::Ptr to the new sub node
     */
    inline TreeNode::Ptr push_item_value(float item_val) noexcept
    {
        return push(item_val);
    }

    /*!
     * \brief push_item_value creates sub Node of Tree with \a item_val
     * \param item_val std::string value for store
     * \return std::shared_ptr<TreeNode> aka TreeNode::Ptr to the new sub node
     */
    inline TreeNode::Ptr push_item_value(const std::string & val) noexcept
    {
        return push(val);
    }

    /*!
     * \brief push_item_value pushes \a item shared pointer to TreeNode as sub Node
     * \param item_val TreeNode::Ptr for store
     * \exception std::runtime_error if \a item is empty pointer
     */
    void push_item_value(TreeNode::Ptr item);

private:

    template<typename T>
    inline TreeNode::Ptr push(T val) noexcept
    {
        m_items.push_back(std::make_shared<TreeNode>(val));
        return m_items.back();
    }

private:
    std::any m_value;
    std::vector<TreeNode::Ptr> m_items;
};

#endif // TREE_H
