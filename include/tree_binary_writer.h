#ifndef TREE_SERIALIZE_H
#define TREE_SERIALIZE_H

#include <ostream>

#include "node_visiter.h"
/*!
 * \brief The TreeBinaryWriter class writes TreeNode to binary format
 *
 * [type_id, value, childrens_number, children_data]
 *
 * type_id - byte with enumeration of type of Node's value. look at type_id.h
 *
 * value - N bytes of value type
 *
 * childrens_number - sizeof(int) bytes of sub nodes's number
 *
 * children_data - serialized children
 */
class TreeBinaryWriter : public NodeVisiter
{
public:
    /*!
     * \brief TreeBinaryWriter constructs Writer with ref to output stream
     * \param out_stream - ref to output stream
     */
    TreeBinaryWriter(std::ostream & out_stream);
    ~TreeBinaryWriter() override;

public:
    void visit(TreeNode *node_ptr, int depth) override;
    void leave(TreeNode *node_ptr, int depth) override;


private:
    void write(char data);
    void write(int data);
    void write(float data);
    void write(const std::string & data);

private:
    std::ostream & m_out_stream;


};


#endif // TREE_SERIALIZE_H
