#ifndef ARGS_PARSER_H
#define ARGS_PARSER_H
#include <string>
#include <map>
#include <vector>

/*!
 * \brief The ArgsParser class parses CLI args to std::map<arg, value>
 */
class ArgsParser
{
public:
    ArgsParser(int argn, char ** argv);

    inline const std::map<std::string, std::string> & get() const
    {
        return m_args;
    }
private:
    std::map<std::string, std::string> m_args;
};
#endif // ARGS_PARSER_H
