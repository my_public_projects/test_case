#ifndef TYPE_ID_H
#define TYPE_ID_H
/*!
 * \brief The type_id enum represents id of types
 */
enum class type_id : unsigned char
{
    _int = 0,
    _float = 1,
    _str = 2
};

#endif // TYPE_ID_H
