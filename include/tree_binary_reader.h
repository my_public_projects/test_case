#ifndef TREE_BINARY_READER_H
#define TREE_BINARY_READER_H
#include <istream>
#include <memory>


#include "tree.h"
#include "type_id.h"

/*!
 * \brief The TreeBinaryReader class reads tree from binary stream
 */
class TreeBinaryReader
{
public:
    TreeBinaryReader() = default;

    /*!
     * \brief read
     * \param in_stream input stream
     * \return shared_ptr to head TreeNode
     * \exception std::runtime_error if input data is broken.
     */
    TreeNode::Ptr read(std::istream & in_stream);

private:

    char read_char(std::istream & in_stream);
    int read_int(std::istream & in_stream);
    float read_float(std::istream & in_stream);
    std::string read_str(std::istream & in_stream);
    type_id read_type_id(std::istream & in_stream);
    TreeNode::Ptr read_node_val(std::istream & in_stream);
};

#endif // TREE_BINARY_READER_H
