#ifndef NODE_VISITER_H
#define NODE_VISITER_H
class TreeNode;
/*!
 * \brief The NodeVisiter abstract class for view all tree nodes
 */
class NodeVisiter
{
public:
    NodeVisiter();

    virtual ~NodeVisiter();

    /*!
     * \brief visit - method will called when node visits
     * \param node_ptr - raw pointer to node for visit
     * \param depth - depth of three from head
     */
    virtual void visit(TreeNode * node_ptr, int depth) = 0;


    /*!
     * \brief leave - method will called when all of subnodes was visited
     * \param node_ptr - raw pointer to leaving node
     * \param depth - depth of three from head
     */
    virtual void leave(TreeNode * node_ptr, int depth) = 0;

};
#endif // NODE_VISITER_H
