#ifndef TREESERIALIZER_H
#define TREESERIALIZER_H
#include "node_visiter.h"


#include <ostream>

/*!
 * \brief The TreeStringify class stringifys Tree with identation
 */
class TreeStringify : public NodeVisiter
{

public:

    TreeStringify(std::ostream & out_stream, signed char ident = 1);

    ~TreeStringify() override;

    void visit(TreeNode *node_ptr, int depth) override;
    void leave(TreeNode *node_ptr, int depth) override;


private:

    std::ostream & m_out_stream;
    signed char m_ident = 1;

};

#endif // TREESERIALIZER_H
