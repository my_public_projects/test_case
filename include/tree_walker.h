#ifndef TREE_STRINGIFY_H
#define TREE_STRINGIFY_H
#include <iostream>

class TreeNode;
class NodeVisiter;
/*!
 * \brief The TreeWalker class for walks by all sub Nodes of Tree
 */
class TreeWalker
{

public:
    TreeWalker() = default;

    /*!
     * \brief walk walks by nodes
     * If tree has circular reference then control will never returned
     *
     * \param tree ref to Tree head Node
     * \param Visiter ref to heir of NodeVisiter class
     */

    //TODO Есть мысль добавить аргумент size_t max_stack_size = 10000
    //на случай если будет циклическая ссылка. Но у этого тоже есть ньюансы...
    void walk(TreeNode & tree, NodeVisiter & Visiter);

};

#endif // TREE_STRINGIFY_H
