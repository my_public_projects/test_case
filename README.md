# test_case
## Tree serialzation/deserialization

My test case from \<X\> company.

The tree nodes can holds: int, float, string.
### Task:
 1. Create a data structure, which represents the tree.
 2. Make Serialization / deserialization of the tree.
 3. Read from input file the tree data and deserialize it. 
 4. Print the tree.
 5. Serialize the tree and write the data to output file.

## requirements
- g++-8
- qmake
## build
    cd test_case
    mkdir build
    cd build
    qmake ../project/tree_serial.pro
    make
## run
    cd test_case/bin
    ./tree_serial -o output -i input    
